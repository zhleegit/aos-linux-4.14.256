#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/sched.h>
#include <linux/pid.h>
#include <linux/cred.h>
#include <linux/time.h>
#include <linux/list.h>
#include <linux/types.h>


struct prinfo {
	long state; /* current state of process */
	long nice; /* process nice value */
	pid_t pid; /* process id */
	pid_t parent_pid; /* process id of parent */
	pid_t youngest_child_pid; /* pid of youngest child */
	unsigned long start_time; /* process start time */
	long long user_time; /* CPU time spent in user mode */
	long long sys_time; /* CPU time spent in system mode */
	long uid; /* user id of process owner */
	char comm[16]; /* name of program executed */
};


// Note for types:

// cred.uid	:kuid_t

// typedef struct {
// 	uid_t val;
// } kuid_t



// asmlinkage long sys_hello(struct prinfo *info)
// SYSCALL_DEFINE1(sys_hello, struct prinfo *, info)
// SYSCALL_DEFINE1(sys_hello, int, info)
// asmlinkage long sys_hello(void);

asmlinkage long sys_hello(struct prinfo *info)
{


	struct prinfo process_info;
	struct task_struct *task_info = current;
	struct task_struct *youngest_child;

	/*
	build process_info
	*/
	process_info.state = task_info->state;
	process_info.nice = task_nice(task_info);
	process_info.pid  = task_info->pid;
	process_info.parent_pid = task_info->parent->pid;

	if (!list_empty(&task_info->children))
	{
		youngest_child = list_last_entry(&task_info->children, struct task_struct , children );
		process_info.youngest_child_pid = youngest_child->pid;
	}
	else
	{
		process_info.youngest_child_pid = -1;
	}

	// process_info.start_time = timespec_to_ns(&task_info->start_time);
	// process_info.user_time = cputime_to_usecs(&task_info->utime);
	// process_info.sys_time = cputime_to_usecs(&task_info->stime);
	process_info.start_time = task_info->start_time;
	process_info.user_time = task_info->utime;
	process_info.sys_time = task_info->stime;
	process_info.uid = task_info->real_cred->uid.val;



	/*
	show process_info
	*/
	printk("=== prinfo ===\n");
	printk("state: %ld\n", process_info.state);
	printk("nice: %ld\n", process_info.nice);
	printk("pid: %d",process_info.pid);
	printk("youngest_child_pid: %d\n",process_info.youngest_child_pid);
	printk("start_time: %lu\n", process_info.start_time);
	printk("user time: %llu\n", process_info.user_time);
	printk("sys time: %llu\n" , process_info.sys_time);

	printk("user time (us): %u\n",(unsigned)(task_info->utime/1000000));
	printk("sys time (us): %u\n", (unsigned)(task_info->stime/1000000));

	printk("comm: %s\n", process_info.comm);
	printk("===============");


	copy_to_user(info , &process_info , sizeof process_info);





	return 0;
}