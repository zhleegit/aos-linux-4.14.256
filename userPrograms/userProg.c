#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>

struct prinfo {
	long state; /* current state of process */
	long nice; /* process nice value */
	pid_t pid; /* process id */
	pid_t parent_pid; /* process id of parent */
	pid_t youngest_child_pid; /* pid of youngest child */
	unsigned long start_time; /* process start time */
	long long user_time; /* CPU time spent in user mode */
	long long sys_time; /* CPU time spent in system mode */
	long uid; /* user id of process owner */
	char comm[16]; /* name of program executed */
};


#define __NR_id 333


int main()
{
	struct prinfo process_info;
	

	long result=syscall(__NR_id, &process_info);
	printf("Test sys call %ld\n", result);

	printf("=== prinfo ===\n");
	printf("state: %ld\n", process_info.state);
	printf("nice: %ld\n", process_info.nice);
	printf("pid: %d",process_info.pid);
	printf("youngest_child_pid: %d\n",process_info.youngest_child_pid);
	printf("start_time: %lu\n", process_info.start_time);
	printf("user time: %llu\n", process_info.user_time);
	printf("sys time: %llu\n" , process_info.sys_time);

//printf("user time direct: %llu\n",task_info->utime);
//printf("sys time direct: %llu\n", task_info->stime);

	printf("comm: %s\n", process_info.comm);
	printf("===============");



	return 0;
}
